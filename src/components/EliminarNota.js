import React from 'react';

const EliminarNota = ({ nota, borrarNota }) => {
    return (
        <>
            <button
                className='btn btn-danger m-3'
                onClick={() => borrarNota(nota)}
            >
                Borrar
            </button>
        </>
    );
};

export default EliminarNota;
