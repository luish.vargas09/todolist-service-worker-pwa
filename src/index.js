import React from 'react';
import ReactDOM from 'react-dom/client';
import AppNotas from './AppNotas';
// import * as serviceWorkerRegistration from './serviceWorkerRegistration';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <>
        <div className='container'>
            <AppNotas />
        </div>
    </>
);

// serviceWorkerRegistration.unregister();
